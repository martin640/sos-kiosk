FROM archlinux:base-devel
RUN pacman-key --init
RUN pacman -Syu --needed --noconfirm xorg xorg-xinit i3-wm i3blocks i3lock i3status \
ttf-dejavu ttf-bitstream-vera noto-fonts ttf-roboto noto-fonts-emoji \
nss atk at-spi2-atk gdk-pixbuf2 gtk3 libxss nginx nodejs npm git

WORKDIR /

COPY . .

CMD ["./install.test.sh"]