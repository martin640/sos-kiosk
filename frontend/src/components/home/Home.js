import React from 'react'
import {withRouter, useHistory} from 'react-router'
import Icon from '@mdi/react'
import { mdiBookOpenPageVariant, mdiApplication, mdiTimetable, mdiMapMarkerPath } from '@mdi/js'
import './Home.css'

import TimetableTimeView from './TimetableTimeView'

const links = [
    { label: 'Stránka školy', icon: mdiApplication, href: 'https://spsbn.edupage.org' },
    { label: 'Rozvrh hodín', icon: mdiTimetable, href: 'https://spsbn.edupage.org/timetable' },
    { label: 'História školy', icon: mdiBookOpenPageVariant, link: '/historia' },
    { label: 'Mapa školy', icon: mdiMapMarkerPath, href: '/map/' },
    { label: 'Jedálny lístok', img: '/res/estrava.gif', href: 'https://secure.ulrichsw.cz/estrava/jidelnicek.php?idzar=177&lang=SK' },
    { label: 'Cestovné poriadky', img: '/res/cpsk.png', href: 'https://cp.hnonline.sk/vlakbusmhdsk/spojenie/' },
    { label: 'Wikipedia', img: '/res/wikipedia-logo.png', href: 'https://wikipedia.org' },
    { label: 'Mapy Google', img: '/res/maps-logo.svg', href: 'https://maps.google.com' },
    { label: 'matura.sk', img: '/res/matura.png', href: 'https://www.matura.sk/priebeh/' },
    { label: 'Ministerstvo školstva', img: '/res/sk_symbol.png', href: 'https://www.minedu.sk/' },
    { label: 'Maturita Info (NÚCEM)', img: '/res/sk_symbol.png', href: 'https://www.nucem.sk/sk/merania/narodne-merania/maturita' },
    { label: 'Portál VŠ', img: '/res/pvs-app.png', href: 'https://www.portalvs.sk/sk/' },
]
const timetableData = [
    { id: 0, start: "7:00", end: "7:45" },
    { id: 1, start: "7:50", end: "8:35" },
    { id: 2, start: "8:40", end: "9:25" },
    { id: 3, start: "9:35", end: "10:20" },
    { id: 4, start: "10:25", end: "11:10" },
    { id: 5, start: "11:15", end: "12:00" },
    { id: 6, start: "12:30", end: "13:15" },
    { id: 7, start: "13:20", end: "14:05" },
    { id: 8, start: "14:25", end: "15:10" },
]

const LargeLinkButton = withRouter((props) => {
    let {link, onClick, children, history} = props;
    if (link) onClick = () => history.push(link);
    return (
        <button onClick={onClick}>
            {children}
        </button>
    )
})

const Home = () => {
    const history = useHistory()
    const [adminCounter, setAdminCounter] = React.useState(0)
    const redirect = (url) => document.location.href = url
    const iconColor = 'inherit'

    function handleAdminCornerClick() {
        setAdminCounter(adminCounter + 1)
        if (adminCounter === 5) {
            history.push('/admin/login')
        }
    }

    return (
        <div className="Home">
            <div className="i1">
                <img src="/assets/logo.png" alt="" />
                <TimetableTimeView table={timetableData} />
            </div>
            <div className="i2">
                {links.map(x => (
                    <LargeLinkButton id={x.href} link={x.link}
                                     onClick={x.href ? () => redirect(x.href) : undefined}>
                        {x.icon && <Icon path={x.icon} color={iconColor} />}
                        {x.img && <img src={x.img} alt="" />}
                        {x.label}
                    </LargeLinkButton>
                ))}
            </div>
            <div className="floating" onClick={handleAdminCornerClick} />
        </div>
    )
}

export default Home
