import React from 'react'
import './TimetableTimeView.css'
import ClockView from "./ClockView";

const timeToMinutes = (str) => {
    let a = str.split(":");
    let b = Number(a[0]) * 60;
    let c = Number(a[1]);
    return b + c;
}

const TimetableTimeViewIndicator = ({timeStart, timeEnd}) => {
    const [, updateState] = React.useState()
    const forceUpdate = React.useCallback(() => updateState({}), [])

    React.useEffect(() => {
        const clockTimerId = setInterval(forceUpdate, 200)
        return () => clearInterval(clockTimerId)
    }, [timeStart, timeEnd])

    const today = new Date()
    const h = today.getHours()
    const minutes = today.getMinutes()
    const seconds = today.getSeconds()

    const minOfDay = (h * 60) + minutes + (seconds / 60)
    const minutesSinceStart = Math.max(minOfDay - timeStart, 0)
    const width = Math.min((minutesSinceStart / (timeEnd - timeStart)) * 100, 100)

    return <div className="day-overview-indicator" style={{width: width + "%"}}/>
}

const TimetableTimeView = ({table}) => {
    if (table.length === 0) return <div className="day-overview"/>
    else {
        const children = []
        const startMin = timeToMinutes(table[0].start)
        const endMin = timeToMinutes(table[table.length - 1].end)
        const totalWidth = endMin - startMin

        for (let i = 0; i < table.length; i++) {
            const d = table[i]
            d.startMin = timeToMinutes(d.start)
            d.endMin = timeToMinutes(d.end)

            const width = ((d.endMin - d.startMin) / totalWidth) * 100
            const left = ((d.startMin - startMin) / totalWidth) * 100

            children.push((
                <div key={d.id} className="day-overview-lesson" style={{width: width + "%", left: left + "%"}}>
                    {d.id}.
                    <small className="day-overview-lesson-times">{d.start}-{d.end}</small>
                </div>
            ))
        }

        return (
            <div>
                <ClockView timetableData={table} />
                <div className="day-overview">
                    {children}
                    <TimetableTimeViewIndicator timeStart={startMin} timeEnd={endMin}/>
                    <div className="day-overview-indicator-full"/>
                </div>
            </div>
        )
    }
}

export default TimetableTimeView
