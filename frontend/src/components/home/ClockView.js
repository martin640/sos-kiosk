import React from 'react'

const checkTime = (i) => {
    if (i < 10) { i = "0" + i }  // add zero in front of numbers < 10
    return i;
}
const timeToMinutes = (str) => {
    let a = str.split(":");
    let b = Number(a[0]) * 60;
    let c = Number(a[1]);
    return b + c;
}
const OPTION_SET_SEC = {o1: "sekundu", oF: "sekundy", oM: "sekúnd"}
const OPTION_SET_MIN = {o1: "minútu", oF: "minúty", oM: "minút"}
const formatNumber = (num, optionSet) => {
    const {o1, oF, oM} = optionSet
    if ((Math.round(num) !== num) || (num > 1 && num < 5)) return `${num} ${oF}`
    else if (num === 1) return `${num} ${o1}`
    else return `${num} ${oM}`
}
const formatTime = (min) => {
    if (min < 1) {
        return formatNumber(Math.round(min * 60), OPTION_SET_SEC);
    } else {
        return formatNumber(Math.round(min), OPTION_SET_MIN);
    }
}

const styles = {
    container: {
        display: 'flex',
        alignItems: 'center',
        gap: 16,
        marginBottom: 8
    },
    clockTime: {
        margin: 0,
        fontWeight: 'bold',
        fontSize: '1.9rem',
        fontFamily: '"WhiteRabbit", monospace'
    },
    timetableInfoText: {
    }
}

const ClockView = (props) => {
    const {timetableData} = props
    const [, updateState] = React.useState()
    const forceUpdate = React.useCallback(() => updateState({}), [])

    React.useEffect(() => {
        const clockTimerId = setInterval(forceUpdate, 200)
        return () => clearInterval(clockTimerId)
    }, [timetableData])

    const today = new Date()

    const h = today.getHours()
    const minutes = today.getMinutes()
    let s = today.getSeconds()
    let m = checkTime(minutes)
    s = checkTime(s)
    const timeStr = h + ":" + m + ":" + s
    const dateStr = today.toLocaleDateString("sk-SK", {
        weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'
    })
    const minOfDay = (h * 60) + minutes + (today.getSeconds() / 60)

    const timetableInfoText = (function () {
        for (let i = 0; i < timetableData.length; i++) {
            const d = timetableData[i]
            d.startMin = timeToMinutes(d.start)
            d.endMin = timeToMinutes(d.end)

            if (minOfDay < d.startMin) {
                return `${d.id}. hodina začína o ${formatTime(d.startMin - minOfDay)}`
            }
            if (minOfDay < d.endMin) {
                return `${d.id}. hodina končí o ${formatTime(d.endMin - minOfDay)}`
            }
        }
        return `${timetableData[0].id}. hodina začína ${today.getDay() === 5 ? 'v pondelok' : 'zajtra'} o ${timetableData[0].start}`
    })();

    return (
        <div style={styles.container}>
            <p style={styles.clockTime}>{timeStr}</p>
            <p style={styles.timetableInfoText}>
                {dateStr}
                <span style={{color: "inherit", opacity: 0.3, padding: "0 8px"}}>―</span>
                {timetableInfoText}
            </p>
        </div>
    )
}

export default ClockView
