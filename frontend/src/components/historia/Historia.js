import React from 'react'
import './Historia.css'

const Historia = () => {
    return (
        <div className="Historia">
            <h1>História školy</h1>
            <img className="image" src="/assets/12191971_884171338304112_5132554494918306812_n.jpg" alt="" />
            <ul>
                <li>
                    <p>Na základe uznesenia Zastupiteľstva Trenčianskeho samosprávneho kraja č. 394/2005 zo dňa 27. 4.
                        2005
                        a č. 466/2005 zo dňa 31.8.2005 zrušením právnickej osoby Stredná priemyselná škola Bánovce nad
                        Bebravou a Stredné odborné učilište odevné Bánovce nad Bebravou, dňom 31.8.2005 spojením
                        Strednej
                        priemyselnej školy so Stredným odborným učilišťom vznikol nový právny subjekt s názvom <strong>Spojená
                            škola, Bánovce nad Bebravou, Farská 7, s organizačnými zložkami Stredná priemyselná škola a
                            Stredné odborné učilište</strong>, ktorú viedol riaditeľ školy Ing. Jozef Orieška. Vznikla
                        nová
                        škola, ktorá mala už v čase svojho vzniku bohatú vyše 50 - ročnú históriu.</p>
                </li>
                <li>
                    <p>V školskom roku 2006/2007 sa naša škola&nbsp;stala bezbariérovou vďaka nainštalovaniu chodiskovej plošiny pre imobilných študentov.</p>
                </li>
                <li>
                    <p>Prebudovaním bývalej strojníckej dielne bola&nbsp;v roku 2007 zriadená nová telocvičňa.</p>
                </li>
                <li>
                    <p>Zmena v názve našej jubilujúcej školy&nbsp;sa udiala k 1.1.2009 na základe
                        uznesenia&nbsp;Zastupiteľstva
                        Trenčianskeho samosprávneho&nbsp;kraja č. 620/2008 zo dňa 29.10.2008. Názov&nbsp;školy sa
                        ustálil na
                        doteraz platnej podobe:&nbsp;<strong>Stredná odborná&nbsp;škola,&nbsp; Farská&nbsp;7, Bánovce
                            nad&nbsp;Bebravou</strong>.
                    </p>
                </li>
                <li>
                    <p>Koncepčné zámery,&nbsp;ako aj plánovanie&nbsp;stratégie školy sa&nbsp;odvíja aj naďalej z potrieb
                        regiónu, záujmov&nbsp;zriaďovateľa, rodičov a žiakov, tradície školy,&nbsp;ale aj konkurenčného
                        prostredia škôl v regióne&nbsp;a podmienok na trhu práce a ich úroveň možno&nbsp;hodnotiť
                        pozitívne.
                        Vedenie školy uplatňovalo&nbsp;a uplatňuje strategické plánovanie smerujúce k rozvoju školy.</p>
                </li>
                <li>
                    <p>Činnosť školy je koordinovaná Radou školy, Radou rodičov, odborovou organizáciou a Študentskou radou.</p>
                </li>
                <li>
                    <p>Na skvalitne­nie vyučovania cudzích jazykov sa od 1.9.2009 využívajú špecializované jazykové
                        laboratóriá. Pre výučbu odborných predmetov (ELE, AUT, PRA) v rámci školských vzdelávacích
                        programov
                        sme vybudovali nové laboratóriá elektrotechniky, hydrau­liky a pneumatiky, ktoré spĺňajú
                        potreb­né
                        náležitosti moder­ného vyučovania.</p>
                </li>
                <li>
                    <p>Pred­met programovanie CNC strojov vyučujeme v odbornej učebni, kde je okrem výpočtovej techniky
                        k
                        dispozícií CNC frézovačka a priemyselný robot zapožičaný firmou&nbsp;Hella Slovakia
                        Signal-Lighting,
                        s.r.o., Hrežďovská 1629/16, Bánovce nad Bebravou.</p>
                </li>
                <li>
                    <p>Škola prežila za uplynulých šesťdesiat rokov svojej histórie ťažkosti aj úspechy, radosti aj
                        smútok.
                        Prešla rôznymi zmenami. Menili sa študijné a učebné odbory, počty tried a menili sa aj žiaci a
                        zamestnanci školy. Nikdy sa však nezmenilo jej dobré meno a najmä cieľ - vychovávať absolventov,
                        ktorí nájdu uplatnenie na trhu práce..&nbsp;</p>
                </li>
                <li>
                    <p>Od 1.9.2016 škola opäť meni názov a teraz aj sídlo na <strong>Stredná odborná škola strojnícka,
                        Partizánska cesta 76, Bánovce nad Bebravou</strong> a sťahuje sa do priestorov po zrušenej
                        Strednej
                        odbornej škole Juraja Ribaya, Partizánska cesta 76, Bánovce nad Bebravou.</p>
                </li>
            </ul>
        </div>
    )
}

export default Historia
