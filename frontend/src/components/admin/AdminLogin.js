import React from 'react'
import {useHistory} from 'react-router'
import './AdminLogin.css'

function AdminLogin() {
    const history = useHistory()
    const [value, setValue] = React.useState('')
    const [error, setError] = React.useState(false)
    const [masked, setMasked] = React.useState(true)
    const valueMasked = masked ? new Array(value.length + 1).join('*') : value

    function submitChar(v) {
        if (value.length < 8) {
            setValue(value + v)
        }
        setError(false)
    }
    function del() {
        if (value.length > 0) {
            setValue(value.substr(0, value.length - 1))
        }
        setError(false)
    }
    function submit() {
        // todo, implement environment variable
        /*if (value === '') {
            history.push('/admin')
        } else*/ setError(true)
    }

    return (
        <div className="AdminLogin">
            <div className="keypad">
                <div className="keypad-input" style={{color: error && '#ff1515'}} onClick={() => setMasked(!masked)}>
                    {valueMasked}
                </div>
                <div className="keypad-key noselect" onClick={() => submitChar('1')}>1</div>
                <div className="keypad-key noselect" onClick={() => submitChar('2')}>2</div>
                <div className="keypad-key noselect" onClick={() => submitChar('3')}>3</div>
                <div className="keypad-key noselect" onClick={() => submitChar('4')}>4</div>
                <div className="keypad-key noselect" onClick={() => submitChar('5')}>5</div>
                <div className="keypad-key noselect" onClick={() => submitChar('6')}>6</div>
                <div className="keypad-key noselect" onClick={() => submitChar('7')}>7</div>
                <div className="keypad-key noselect" onClick={() => submitChar('8')}>8</div>
                <div className="keypad-key noselect" onClick={() => submitChar('9')}>9</div>
                <div className="keypad-key noselect" onClick={() => del()}>⮨</div>
                <div className="keypad-key noselect" onClick={() => submitChar('0')}>0</div>
                <div className="keypad-key noselect" onClick={() => submit()}>🡺</div>
            </div>
        </div>
    )
}

export default AdminLogin
