import React from 'react'
import './Admin.css'

function humanFileSize(bytes, si=false, dp=1) {
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) return bytes + ' B';

    const units = si
        ? ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
        : ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let u = -1;
    const r = 10**dp;

    do {
        bytes /= thresh; ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);

    return bytes.toFixed(dp) + ' ' + units[u];
}

function Admin() {
    const [nets, setNets] = React.useState(undefined)
    const [cpu, setCpu] = React.useState(undefined)
    const [cpuTemp, setCpuTemp] = React.useState(undefined)
    const [mem, setMem] = React.useState(undefined)
    const [error, setError] = React.useState(undefined)

    const fallbackRow = (
        <img src="https://raw.githubusercontent.com/Codelessly/FlutterLoadingGIFs/master/packages/cupertino_activity_indicator.gif"
             alt="" style={{height: 16}}/>
    )

    React.useEffect(() => {
        if (window.kiosk) {
            const {nets, cpu, cpuTemp, mem} = window.kiosk.fetchAdminData()
            setNets(nets)
            cpu.then(x => setCpu(x))
            cpuTemp.then(x => setCpuTemp(x))
            mem.then(x => setMem(x))
        } else setError('kiosk api nie je k dispozícii')
    }, [])

    if (error) {
        return (
            <div className="Admin">
                <p style={{color: '#d23c3c'}}>{error}</p>
            </div>
        )
    } else {
        return (
            <div className="Admin">
                <div style={{marginBottom: 16}}>
                    <button onClick={() => window.kiosk.restartKiosk()}>Restart kiosk</button>
                    <button onClick={() => window.kiosk.restartSystem()}>Restart system</button>
                    <button onClick={() => window.kiosk.toggleDevtools()}>Toggle devtools</button>
                </div>
                <table style={{marginBottom: 16}}>
                    <tr>
                        <th>Interface</th>
                        <th>IP address</th>
                        <th>Subnet mask</th>
                        <th>Hardware address</th>
                    </tr>
                    {nets && nets.map(val => (
                        <tr key={val.name}>
                            <td>{val.name}</td>
                            <td>{val.address}</td>
                            <td>{val.netmask}</td>
                            <td>{val.mac}</td>
                        </tr>
                    ))}
                </table>
                <table style={{marginBottom: 16}}>
                    <tr>
                        <th>Kľúč</th>
                        <th>Hodnota</th>
                    </tr>
                    <tr>
                        <td>Teplota procesora</td>
                        <td>
                            {cpuTemp ? `${cpuTemp.main >= 0 ? cpuTemp.main : '---'} °C` : fallbackRow}
                        </td>
                    </tr>
                    <tr>
                        <td>Procesor</td>
                        <td>
                            {cpu ? `${cpu.manufacturer} ${cpu.brand} (${cpu.cores}) @ ${cpu.speed} GHz` : fallbackRow}
                        </td>
                    </tr>
                    <tr>
                        <td>Pamäť</td>
                        <td>
                            {mem ? `${humanFileSize(mem.used)} / ${humanFileSize(mem.total)}` : fallbackRow}
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

export default Admin
