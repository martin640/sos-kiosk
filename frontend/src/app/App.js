import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Home from '../components/home/Home'
import Historia from '../components/historia/Historia'
import AdminLogin from '../components/admin/AdminLogin'
import Admin from '../components/admin/Admin'

export default function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/admin" component={Admin} />
                <Route exact path="/admin/login" component={AdminLogin} />
                <Route exact path="/historia" component={Historia} />
            </Switch>
        </Router>
    )
}
