const { ipcRenderer, contextBridge } = require('electron')
const { spawn } = require('child_process')

const {networkInterfaces} = require('os')
const si = require('systeminformation')

Number.round = (num, precision) => {
    const a = Math.pow(10, precision)
    return Math.round(num * a) / a
}

function racePromise(promise, timeout) {
    return new Promise((resolve, reject) => {
        const timeoutId = setTimeout(() => reject(new Error('Timed out')), timeout)
        const clear = () => clearTimeout(timeoutId)
        promise.then(res => clear(resolve(res))).catch(e => clear(reject(e)))
    })
}

contextBridge.exposeInMainWorld(
    "kiosk", {
        exit: () => { ipcRenderer.send('nav', 'exit') },
        reload: () => { ipcRenderer.send('nav', 'reload') },
        back: () => { ipcRenderer.send('nav', 'back') },
        home: () => { ipcRenderer.send('nav', 'home') },
        about: () => { ipcRenderer.send('nav', 'about') },
        toggleDevtools: () => { ipcRenderer.send('nav', 'devtools') },
        version: () => ipcRenderer.sendSync('nav', 'version'),
        handleUrl: (handler) => {
            ipcRenderer.on('url_change', (e, message) => handler(message))
        },
        handleLoading: (handler) => {
            ipcRenderer.on('loading_change', (e, message) => handler(message))
        },
        fetchAdminData: () => {
            const netsRaw = networkInterfaces()
            const cpu = si.cpu()
            const cpuTemp = si.cpuTemperature()
            const mem = si.mem()

            const nets = []
            for (const name of Object.keys(netsRaw)) {
                for (const net of netsRaw[name]) {
                    if (net.family === 'IPv4' && !net.internal) {
                        net.name = name
                        nets.push(net)
                    }
                }
            }

            return { nets, cpu, cpuTemp, mem }
        },
        restartKiosk: () => {
            const child = spawn('/home/guest/kiosk/scripts/run.sh', { detached: true })
            child.unref()
        },
        restartSystem: () => {
            const child = spawn('systemctl', ['reboot', '-i'], { detached: true })
            child.unref()
        }
    }
)
