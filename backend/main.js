const { app, ipcMain, BrowserWindow, BrowserView, screen, session } = require('electron')
const path = require('path')
const fs = require('fs')
const URL = require('url').URL

// Constants
const FLAG_SIDEBAR_DEBUG = false
const Strings = {
    CHANNEL_INCOME: 'main',
    CHANNEL_URL_CHANGE: 'url_change',
    CHANNEL_LOADING: 'loading_change',
    HOME_URL: 'http://localhost:3000',
    ERR_IURL: '<span style="color: #ff4040">Chyba navigácie</span>'
}
const URL_WHITELIST = require(path.join(__dirname, 'url_whitelist.json'))
const Metrics = { }
Metrics.sidebarHeight = FLAG_SIDEBAR_DEBUG ? 500 : 80
// End of constants

// helper functions
function now() { const hrTime = process.hrtime(); return (hrTime[0] * 1000000 + hrTime[1] / 1000) / 1000 }
function round(num, decimals) { let a = Math.pow(10, decimals); return Math.round(num * a) / a }

// runtime variables
let _win, sidebar, browser
let returnTimer
const start = now()
const injectScript = fs.readFileSync(path.join(__dirname, "__inject.js"), 'utf-8')

/**
 * Checks url against given rule
 * @param parsedUrl {URL} parsed {@link #url}
 * @param url {string} full url string
 * @param i {number} numeric index of rule
 * @param rule {Object} object containing matching definition
 * @returns {boolean} true if url matches rule
 */
function matchRule(parsedUrl, url, i, rule) {
    let src, pass
    switch (rule.src) {
        case "url": src = url; break
        case "origin": src = parsedUrl.origin; break
        default: console.warn(`Unknown ruleset object src: ${rule.src} [at index ${i}] :: skipping...`); return false
    }
    switch (rule.match) {
        case "exact": pass = (rule.filter === src); break
        case "start": pass = (src.startsWith(rule.filter)); break
        case "end": pass = (src.endsWith(rule.filter)); break
        case "contains": pass = (src.includes(rule.filter)); break
        default: console.warn(`Unknown ruleset object match: ${rule.match} [at index ${i}] :: skipping...`); return false
    }
    if (pass && rule.and) pass = matchRule(parsedUrl, url, i, rule.and)
    return pass
}

/**
 * Matches url against given ruleset (in whitelist mode)
 * @param url {string} full url string
 * @returns {boolean} true if url matches any rule
 */
function allowUrl(url) {
    const parsedUrl = new URL(url)

    for (let i = 0; i < URL_WHITELIST.length; i++) {
        const rule = URL_WHITELIST[i]
        if (matchRule(parsedUrl, url, i, rule)) return true
    }
    console.warn(`No matching rule for url \"${url}\" found in the whitelist :: rejecting request`)
    return false
}

function escapeHtml(unsafe) {
    return unsafe.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll("'", '&#039;');
}

/**
 * Parses url into colored html representation for address bar
 * @param url {string}
 * @returns {string} output html
 */
function parseUrlHtml(url) {
    if (url.startsWith('file://') || url.includes('localhost:3000')) return ''
    const urlData = new URL(escapeHtml(url))
    const protocol = (urlData.protocol === 'http:') ? `<u style="color: #ff4040">${urlData.protocol}</u>` : urlData.protocol
    const auth = (urlData.username && urlData.password) ? `${urlData.username}:${urlData.password}@` : `${urlData.username}${urlData.password}`
    const auth2 = auth ? `${auth}@` : auth
    const host = `${urlData.hostname}${urlData.port ? ':' : ''}${urlData.port}`
    return `${protocol}//${auth2}<span style="color: #3890ff">${host}</span>${urlData.pathname}${urlData.search}${urlData.hash}`
}

/**
 * Common handler for events updating location address
 * @param e {Event} original event. If truthy, function will call {@link Event#preventDefault()} on it.
 * @param url {string}
 * @param handleRedirect {boolean} if true, function will take care of navigating to requested url
 * @returns {boolean} true if url is allowed and can be navigated to
 */
function navigationHandler(e, url, handleRedirect = true) {
    if (typeof returnTimer !== 'undefined') clearTimeout(returnTimer)
    if (!url.includes(Strings.HOME_URL)) {
        returnTimer = setTimeout(() => browser.webContents.loadURL(Strings.HOME_URL), 300000)
    }

    if (!allowUrl(url)) {
        if (e) e.preventDefault()
        browser.webContents.loadURL(`file://${__dirname}/assets/error.html?err=1&url=${encodeURIComponent(url)}`)
        return false
    } else if (handleRedirect) {
        browser.webContents.loadURL(url)
    }
    return true
}

function createWindow() {
    const { width, height } = screen.getPrimaryDisplay().size
    const options = {
        kiosk: true,
        webPreferences: {
            nodeIntegration: false,
            contextIsolation: true,
            enableRemoteModule: false,
            preload: path.join(__dirname, "preload.js")
        }
    }

    _win = new BrowserWindow(options)
    _win.maximize()
    _win.setFullScreen(true)
    _win.removeMenu()

    sidebar = new BrowserView(options)
    _win.addBrowserView(sidebar)
    sidebar.setBounds({ x: 0, y: 0, width, height: Metrics.sidebarHeight })
    if (FLAG_SIDEBAR_DEBUG) {
        sidebar.webContents.openDevTools()
    }

    browser = new BrowserView(options)
    _win.addBrowserView(browser)
    browser.setBounds({ x: 0, y: Metrics.sidebarHeight, width: width, height: height - Metrics.sidebarHeight })
    browser.webContents.openDevTools()

    browser.webContents.on('will-attach-webview', (e) => e.preventDefault())
    browser.webContents.on('will-navigate', (e, url) => navigationHandler(e, url, false))
    browser.webContents.setWindowOpenHandler(handler => {
        navigationHandler(null, handler.url)
        return { action: 'deny' }
    })
    browser.webContents.on('did-navigate-in-page', (_, url) => sidebar.webContents.send(Strings.CHANNEL_URL_CHANGE, parseUrlHtml(url)))
    browser.webContents.on('did-navigate', (_, url) => sidebar.webContents.send(Strings.CHANNEL_URL_CHANGE, parseUrlHtml(url)))
    browser.webContents.on('did-start-loading', () => sidebar.webContents.send(Strings.CHANNEL_LOADING, true))
    browser.webContents.on('did-stop-loading', () => sidebar.webContents.send(Strings.CHANNEL_LOADING, false))

    browser.webContents.on('did-finish-load', () => {
        const currentURL = browser.webContents.getURL()
        console.log('url:', currentURL)
        if (currentURL.includes('spsbn.edupage.org/timetable')) {
            browser.webContents.insertCSS(`
                .l-page-width { width: 1500px !important; }
                .l-page-width.no-padding { width: 1500px !important; }
                .secondary-page .entry-container-1,
                .secondary-page .entry-container-2,
                .secondary-page .entry-container-3 { background-image: unset !important; }
            `)
        }
    })
    browser.webContents.on('did-fail-load', (_, errCode, errDescription, url) => {
        if (errCode < -1) {
            browser.webContents.loadURL(`file://${__dirname}/assets/error.html?err=${-errCode}&err_msg=${encodeURIComponent(errDescription)}&url=${encodeURIComponent(url)}`)
        }
    })
    browser.webContents.on('dom-ready', () => {
        browser.webContents.executeJavaScript(injectScript)
    })

    session.defaultSession.on('will-download', (event, item, webContents) => {
        event.preventDefault()
    })

    browser.webContents.loadURL(Strings.HOME_URL)
    sidebar.webContents.loadFile('assets/sidebar.html')
    console.log(`Window has been created in ${round(now() - start, 2)} ms`)
}

app.whenReady().then(createWindow)

ipcMain.on('nav', (event, action, ...data) => {
    // Kiosk API calls
    if (action === "home") {
        console.log("Received message to navigate home")
        browser.webContents.loadURL(Strings.HOME_URL)
        browser.webContents.clearHistory()
    } else if (action === "back") {
        console.log("Received message to navigate back")
        browser.webContents.goBack()
    } else if (action === "reload") {
        console.log("Received message to reload page")
        browser.webContents.reload()
    } else if (action === "devtools") {
        browser.webContents.toggleDevTools()
    } else if (action === "version") {
        event.returnValue = app.getVersion()
    } else if (action === "exit") {
        process.exit(0)
    }
})

app.on('web-contents-created', (_, contents) => {
    contents.on('will-navigate', (e, url) => navigationHandler(e, url, false))
})
