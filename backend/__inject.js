const head = document.getElementsByTagName('head')[0];

let link = document.createElement('link');
link.type = 'text/css';
link.rel = 'stylesheet';
link.href = 'https://cdn.jsdelivr.net/npm/kioskboard@1.3.3/dist/kioskboard-1.3.3.min.css';
head.appendChild(link);

const kioskBoardScript = document.createElement('script');
kioskBoardScript.async = false;
kioskBoardScript.src = 'https://cdn.jsdelivr.net/npm/kioskboard@1.3.3/dist/kioskboard-aio-1.3.3.min.js';
kioskBoardScript.addEventListener('load', () => {
    const activeElement = document.activeElement
    activeElement && activeElement.blur()
    KioskBoard.Init({
        keysArrayOfObjects: [
            { "0": "Q", "1": "W", "2": "E", "3": "R", "4": "T", "5": "Z", "6": "U", "7": "I", "8": "O", "9": "P" },
            { "0": "A", "1": "S", "2": "D", "3": "F", "4": "G", "5": "H", "6": "J", "7": "K", "8": "L" },
            { "0": "Y", "1": "X", "2": "C", "3": "V", "4": "B", "5": "N", "6": "M" }
        ],
        language: 'sk',
        theme: 'light',
        capsLockActive: false,
        allowRealKeyboard: true,
        cssAnimations: true,
        cssAnimationsDuration: 360,
        cssAnimationsStyle: 'slide',
        keysAllowSpacebar: true,
        keysSpacebarText: 'Space',
        keysFontFamily: 'sans-serif',
        keysFontSize: '22px',
        keysFontWeight: 'normal',
        keysIconSize: '25px',
        allowMobileKeyboard: true,
        autoScroll: true,
    });
    KioskBoard.Run('input[type=email], input[type=number], input[type=password], input[type=search], input[type=tel], input[type=text], input[type=url], textarea');
});
head.appendChild(kioskBoardScript);
