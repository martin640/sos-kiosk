#!/usr/bin/env sh
OUT_PREFIX="install.sh"
USER="guest" # Do not change!

title() { MSG=$@ ; printf '\n\n'; echo -e "\e[1m\e[34m$MSG\e[0m "; }
log() { MSG=$@ ; echo -e "[\e[1m\e[92m$OUT_PREFIX\e[0m] \e[93m$MSG\e[0m " ; }
warn() { MSG=$@ ; echo -e "[\e[1m\e[92m$OUT_PREFIX\e[0m] \e[43m\e[97m$MSG\e[0m " ; }
panic() { MSG=$@ ; echo -e "[\e[1m\e[92m$OUT_PREFIX\e[0m] \e[41m\e[97m$MSG\e[0m " ; exit 1 ; }
#cat <<EOT > /home/${USER}/displaycfg.sh
#EOT

cd "$(dirname "$0")" || panic "Failed to cd to the script directory"
ORIGIN=$(pwd)
log "Origin directory: $ORIGIN"

title " :: Creating required users and groups"
if [[ ! -d "/home/${USER}" ]]; then
  useradd -m ${USER}
  groupadd video
  mkdir /home/${USER}/kiosk
fi

title " :: Updating repository..."
log "Pulling changes..."
git reset --hard
git pull

# copy system config
log "Copying files..."
cp -fa ./root/. / || panic "Failed to copy system config"
# cp --parents -au ./scripts/root / || panic "Failed to copy system config"
rm -rf /home/${USER}/kiosk/backend /home/${USER}/kiosk/frontend /home/${USER}/kiosk/scripts
cp -fr ./backend /home/${USER}/kiosk/ || panic "Failed to copy source code"
cp -fr ./frontend /home/${USER}/kiosk/ || panic "Failed to copy source code"
cp -fr ./scripts /home/${USER}/kiosk/ || panic "Failed to copy source code"

log "Installing npm dependencies..."
cd /home/${USER}/kiosk/frontend || panic "Unexpected error"
rm -rf node_modules
rm -f package-lock.json
npm install || panic "Failed to install frontend dependencies"
npm run build || panic "Frontend build failed"
cd /home/${USER}/kiosk/backend || panic "Unexpected error"
rm -rf node_modules
rm -f package-lock.json
npm install || panic "Failed to install backend dependencies"
cd $ORIGIN || panic "Unexpected error"

log "Checking permissions..."
chmod +x perms_fix.sh
./perms_fix.sh
usermod -aG video ${USER}

log "All checks passed"
