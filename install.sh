#!/usr/bin/env sh
DESKTOP="xorg xorg-xinit i3-wm i3blocks i3lock i3status"
FONTS="ttf-dejavu ttf-bitstream-vera noto-fonts ttf-roboto noto-fonts-emoji"
DEPENDENCIES="nss atk at-spi2-atk gdk-pixbuf2 gtk3 libxss nginx nodejs npm $FONTS $DESKTOP"
OUT_PREFIX="install.sh"
USER="guest" # Do not change!

title() { MSG=$@ ; printf '\n\n'; echo -e "\e[1m\e[34m$MSG\e[0m "; }
log() { MSG=$@ ; echo -e "[\e[1m\e[92m$OUT_PREFIX\e[0m] \e[93m$MSG\e[0m " ; }
warn() { MSG=$@ ; echo -e "[\e[1m\e[92m$OUT_PREFIX\e[0m] \e[43m\e[97m$MSG\e[0m " ; }
panic() { MSG=$@ ; echo -e "[\e[1m\e[92m$OUT_PREFIX\e[0m] \e[41m\e[97m$MSG\e[0m " ; reboot ; exit 1 ; }
#cat <<EOT > /home/${USER}/displaycfg.sh
#EOT

cd "$(dirname "$0")" || panic "Failed to cd to the script directory"
ORIGIN=$(pwd)
log "Origin directory: $ORIGIN"

# Check dependencies
title " :: Preparing to unpack"
log "Checking dependencies..."
pacman -Qi $DEPENDENCIES &>/dev/null || {
  log "Installing dependencies..."
  pacman -Syu --needed --noconfirm $DEPENDENCIES || panic "Failed to install dependencies"
}

title " :: Creating required users and groups"
if [[ ! -d "/home/${USER}" ]]; then
  useradd -m ${USER}
  groupadd video
  mkdir /home/${USER}/kiosk
fi

title " :: Updating repository..."
log "Pulling changes..."
git reset --hard
git pull

# copy system config
log "Copying files..."
cp -fa ./root/. / || panic "Failed to copy system config"
# cp --parents -au ./scripts/root / || panic "Failed to copy system config"
rm -rf /home/${USER}/kiosk/backend /home/${USER}/kiosk/frontend /home/${USER}/kiosk/scripts
cp -fr ./backend /home/${USER}/kiosk/ || panic "Failed to copy source code"
cp -fr ./frontend /home/${USER}/kiosk/ || panic "Failed to copy source code"
cp -fr ./scripts /home/${USER}/kiosk/ || panic "Failed to copy source code"

log "Installing npm dependencies..."
cd /home/${USER}/kiosk/frontend || panic "Unexpected error"
if [[ ! -f "/root/_kiosk_" ]]; then
  # clear modules and package lock
  rm -rf node_modules
  rm -f package-lock.json
fi
npm install || panic "Failed to install frontend dependencies"
npm run build
cd /home/${USER}/kiosk/backend || panic "Unexpected error"
if [[ ! -f "/root/_kiosk_" ]]; then
  # clear modules and package lock
  rm -rf node_modules
  rm -f package-lock.json
fi
npm install || panic "Failed to install backend dependencies"
cd $ORIGIN || panic "Unexpected error"
touch /root/_kiosk_

log "Checking permissions..."
chmod +x perms_fix.sh
./perms_fix.sh
usermod -aG video ${USER}

log "Configuring kiosk-update service..."
systemctl daemon-reload
systemctl enable kiosk-update.timer

log "Configuring nginx..."
systemctl enable nginx

reboot
