# Prispievanie k zdrojovému kódu

Kiosk je určený pre všetkých a tak isto zdrojový kód kiosku je verejný a môže ho ktokoľvek upravovať podľa aktuálnych potrieb.

## Špecifikácie a zásady pri upravovaní
- Projekt sa zkladá z 3 častí:
  - `backend` je naprogramovaný v Javascripte (Node.js) a stará sa o zobrazovanie upraveného prehliadača (Electron) a kontrolu prístupu k webovým stránkam
  - `frontend` je naprogramovaný v Javascripte (Node.js/React) s poskytuje domovskú stránku kiosku s odkazmi na najpoužívanejšie stránky
  - `root` obsahuje očakávanú konfiguráciu systému (projekt je vytvorený pre Arch Linux) vrátane konfigurácie systemd a nginx
- `frontend` by mal byť neutrálny a spustiteľný aj z normálneho prehliadača (to znamená že by nemal závisieť na Node.js runtime premenných ani na API backendu)
- `backend` by nemal umožňovať prístup na neoverené stránky, teda by sa mal riadiť podľa whitelistu [[zdroj](https://gitlab.com/martin640/sos-kiosk/-/blob/main/backend/url_whitelist.json)]
- V konfigurácii je časovač ktorý aktualizuje zdrojový kód kiosku z Git-u a reštartuje systém každý deň o ~17:00 [[zdroj](https://gitlab.com/martin640/sos-kiosk/-/blob/main/root/etc/systemd/system/kiosk-update.timer)]
- V repozitári je [Dockerfile](https://gitlab.com/martin640/sos-kiosk/-/blob/main/Dockerfile) pre vytvorenie Docker kontainera ktorý overí funkčnosť inštalačného skriptu preto zmeny v [install.sh](https://gitlab.com/martin640/sos-kiosk/-/blob/main/install.sh) by mali byť adekvátne zrkadlené do [install.test.sh](https://gitlab.com/martin640/sos-kiosk/-/blob/main/install.test.sh)

## Lokálne spustenie projektu

frontend

```
cd backend
npm install
npm start
```

backend (**musí bežať frontend na porte 3000**)

```
cd backend
npm install
npm start
```
