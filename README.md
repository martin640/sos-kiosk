# Kiosk SOŠ Strojnícka BN

![screenshot](/screenshot1.png?raw=true)

## Inštalácia kiosku

Inštalačný skript je vytvorený pre Arch Linux a je potrebné ho spusiť z účtu roota.

```
chmod +x ./install.sh
./install.sh
```
